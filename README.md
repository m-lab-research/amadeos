# AMADEOS

An Online System for Automated Model-driven Database Design

#### Links
* [M-lab](http://m-lab.etf.unibl.org)
* [AMADEOS](http://m-lab.etf.unibl.org:8080/amadeos/)
* [AMADEOS generator](http://m-lab.etf.unibl.org:8080/amadeos/generator.html)

#### Samples and tools
You can try AMADEOS with the source models that we have prepared or you can create your own models.
You can use the [Eclipse Papyrus](https://www.eclipse.org/papyrus/) modeling environment and [BPMN2 Modeler](https://www.eclipse.org/bpmn2-modeler/) Eclipse plug-in.

#### Usage
AMADEOS can be used in two ways:
1. Following the links above, you can access a page where it is possible to upload the source bussiness process model and download the generated database model or visualise and edit it directly in the browser.
2. You can implement your own application which consumes the exposed REST service for generation of the target database model. Here we provide an example of the Web service client (Java):

```java
FileDataBodyPart filePart = new FileDataBodyPart("input", new File("path_to_source_model"));

FormDataMultiPart multipart = new FormDataMultiPart();
multipart.field("source_model_type", "AD").bodyPart(filePart);
// For BPMN: multipart.field("source_model_type", "BPMN").bodyPart(filePart);

ClientConfig clientConfig = new ClientConfig().register(MultiPartFeature.class);
Client client = ClientBuilder.newClient(clientConfig);
WebTarget target = client.target("http://m-lab.etf.unibl.org:8080/amadeos/services/").path("generate").path("cdm");

Response response = target.request().post(Entity.entity(multipart, multipart.getMediaType()));
if (response.getStatus() == 200) {
    InputStream is = response.readEntity(InputStream.class);
    File f = new File("path_to_target_model.uml");
    FileUtils.copyToFile(is, f);
    is.close();
}

filePart.cleanup();
multipart.close();
client.close();
response.close();
```